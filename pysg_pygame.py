import PySimpleGUI as sg
import os
import pygame



def run():
    
    # -------- CALLING CLASS FOR PYESIMPLEGUI WINDOW ------------------
    window = simple_win() 
    window.return_simple_win()
    


    # -------------- Magic code to integrate PyGame with tkinter -------
    # change x11 to 'windib' to make it work on Windows
    os.environ['SDL_VIDEODRIVER'] = 'x11'
   
        
    
    # -------------- Variable initialization ------------------------
    pos = [320, 240] 
    r = 125      
    pygame_on = False
        
        
    
    # -------------- THE SCENE LOOP BEGINS --------------------------------
    while True:
        event, values = window.return_simple_win().read(timeout=10)
        
        
        
        # ---------------------LOOP FOR PYESIMPLEGUI EVENT ----------------------------------------
        if event in (sg.WIN_CLOSED, 'EXIT'): 
            pygame.quit()
            break
                
        elif event == 'DRAW':
            pygame_on = True # VARIABLE TO START PYGAME LOOP ---------------------------------------
            program = Pygame_screen()
            program.run()
        elif event == '-R-':
            r = values['-R-']
        elif event == '-H-':
            pos[1] = (500-values['-H-'])
        
        elif event == 'SX':
            pos[0] = (pos[0]-10)
        elif event == 'DX':
            pos[0] = (pos[0]+10)
        
        col = values['-OPTION MENU-']
        
        
        
        # ---------------------LOOP FOR PYEGAME EVENT ----------------------------------------
        if pygame_on:
            program.update(r, pos, col)              

            program.draw()
            program.clock.tick(60)     
        
    
    window.return_simple_win().close() # END LOOP --------------------------



class simple_win(): #-------- pysimplegui code -------------------------------------
    def __init__(self):
        sg.theme('dark blue 13')
        
        layout_gestione = [[sg.Push(), sg.Button('DRAW', k='DRAW'), sg.Push()],
                           [sg.VPush()],
                           [sg.Push(), sg.OptionMenu(values=('BLUE', 'GREEN', 'BLACK'), default_value = 'BLUE', k='-OPTION MENU-'), sg.Push()],
                           [sg.Push(), sg.Slider(orientation='h', range = (10, 200),
                                      default_value = 100,
                                      resolution = 10,
                                      disable_number_display = True,
                                      border_width = 2,
                                      relief = 'flat',
                                      enable_events = True,
                                      pad = (1,30),
                                      key='-R-'),
                           sg.Slider(orientation='v', range = (10, 450),
                                      default_value = 250,
                                      resolution = 10,
                                      disable_number_display = True,
                                      border_width = 2,
                                      relief = 'flat',
                                      enable_events = True,
                                      pad = (1,30),
                                      key='-H-'), sg.Push()],          
                           [sg.Push(), sg.Button('SX', k='SX'), (sg.Button('DX', k='DX')), sg.Push()],                           
                           [sg.Push(), sg.Button('EXIT'), sg.Push()]]
        
       
        
        
        layout = [[sg.Frame('Frame', layout_gestione, relief = 'flat', s=(300,380))]]
        
        
        self.window =sg.Window('pysimplegui_and_pygame',
                               layout,
                               relative_location = (0,400),
                               alpha_channel = 0.95,
                               grab_anywhere=True,
                               margins=(0,0),
                               no_titlebar=True,
                               finalize=True,
                               keep_on_top=True)
    
    
    def return_simple_win(self):
        return self.window


class Pygame_screen(): #-------- pygames code ----------------------------------------------
    def __init__(self):
        pygame.init()
        self.clock = pygame.time.Clock()
        self.screen = pygame.display.set_mode((640, 480), pygame.NOFRAME)
        
        self.sfondo = pygame.Surface((640, 480))
        self.sfondo.fill(pygame.Color(0, 34, 51))

               

    
    def run(self):
        self.circle = pygame.draw.circle(self.screen, (0, 0, 0), (250, 250), 125)
    
     
    def update(self, r, pos, col):
        self.r = r
        self.pos = pos
        self.col = col
        


    # def get_event () replaced with event handling in the pysimplegui loop ------------------
    
    
    
    def draw(self):
        self.screen.blit(self.sfondo, (0, 0)) #coloro lo sfondo
        self.circle = pygame.draw.circle(self.screen, (self.col), (self.pos), self.r)     


        pygame.display.update()





if __name__ == '__main__':
    run()